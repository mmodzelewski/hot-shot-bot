const fs = require('fs');
const puppeteer = require('puppeteer-core');
const request = require('request');

function getOptionsForSendingPhoto() {
  return {
    method: "POST",
    url: "https://api.telegram.org/bot<token>/sendPhoto",
    headers: {
      "Content-Type": "multipart/form-data"
    },
    formData: {
      chat_id: "",
      photo: fs.createReadStream("/latest/hotShot.png")
    }
  }
}

function getOptionsForSendingMessage(url) {
  return {
    method: "POST",
    url: "https://api.telegram.org/bot<token>/sendMessage",
    json: true,
    body: {
      chat_id: "",
      text: url,
      disable_web_page_preview: true,
      disable_notification: true
    }
  }
}

function requestHandler(err, res, body) {
  if (err) {
    console.log(err);
  }
  console.log(body);
}


(async () => {
  let latestId;
  try {
    latestId = fs.readFileSync('/latest/id', 'utf-8');
  } catch (e) {
    console.log('no previous product');
  }
  const browser = await puppeteer.launch({ args: ['--disable-dev-shm-usage'], executablePath: "google-chrome-unstable" });
  const page = await browser.newPage();
  await page.goto('https://x-kom.pl');

  const hotShot = await page.$('#hotShot');
  const productId = await hotShot.$eval('.product-impression', node => node.dataset.productId);

  if (latestId != productId) {
    await hotShot.screenshot({ path: '/latest/hotShot.png' });
    await hotShot.click();
    const hotShotUrl = (await browser.waitForTarget(target => target.url().indexOf('goracy_strzal') >= 0)).url();

    console.log(productId);
    fs.writeFileSync('/latest/id', productId, 'utf-8');
    request(getOptionsForSendingPhoto(), requestHandler);
    request(getOptionsForSendingMessage(hotShotUrl), requestHandler);
  }

  await browser.close();
})();
