#!/bin/sh

docker container rm hotshot
docker run --init --cap-add=SYS_ADMIN --name hotshot -d mmodzelewski/hot-shot
docker logs -f hotshot
